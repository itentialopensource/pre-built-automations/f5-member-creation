
## 0.0.12 [07-08-2022]

* Certification for 2022.1

See merge request itentialopensource/pre-built-automations/f5-member-creation!6

---

## 0.0.11 [12-16-2021]

* Update to 2021.2

See merge request itentialopensource/pre-built-automations/f5-member-creation!5

---

## 0.0.10 [11-15-2021]

* Update pre-built description

See merge request itentialopensource/pre-built-automations/f5-member-creation!4

---

## 0.0.9 [07-09-2021]

* Certify on IAP 2021.1

See merge request itentialopensource/pre-built-automations/f5-member-creation!3

---

## 0.0.8 [03-23-2021]

* Update README.md

See merge request itential/sales-engineer/selabprebuilts/f5-membercreation!5

---

## 0.0.7 [03-09-2021]

* Update README.md

See merge request itential/sales-engineer/selabprebuilts/f5-membercreation!5

---

## 0.0.6 [03-04-2021]

* Update README.md, images/f5_member_mgmt_canvas.png files

See merge request itential/sales-engineer/selabprebuilts/f5-membercreation!4

---

## 0.0.5 [02-24-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/f5-membercreation!1

---

## 0.0.4 [02-22-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/f5-membercreation!1

---

## 0.0.3 [02-17-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/f5-membercreation!1

---

## 0.0.2 [01-29-2021]

* Bug fixes and performance improvements

See commit 27862d7

---
